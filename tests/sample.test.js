const app = require('../app.js') // Link to your server file
const supertest = require('supertest')
const request = supertest(app)

describe('GET /', () => {
  it('gets the test endpoint', async done => {
    const response = await request.get('/');
    expect(response.status).toBe(200)
    //Search especific text in the response
    const position = response.text.search("Express - Node2.js Express Application");
    // if position = -1 => text not found
    // if position >= 0 => text found
    expect(position == -1).toBe(false)
    done()
  })
})